//<?php
///**
// * @file
// * Forms for creating, editing, and deleting products.
// */
//
//
///**
// * Form callback: create or edit a product.
// *
// * @param $product
// *   The product object to edit or for a create form an empty product object
// *     with only a product type defined.
// */
//function commerce_package_product_form($form, &$form_state, $product) {
//  $language = !empty($product->language) ? $product->language : LANGUAGE_NONE;
//
//  // Ensure this include file is loaded when the form is rebuilt from the cache.
//  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'commerce_package') . '/includes/commerce_package.forms.inc';
//
//  // Add the default field elements.
//  // TODO: Update description to include the acceptable product tokens.
//  $form['weight'] = array(
//    '#type' => 'textfield',
//    '#title' => t('Product Weight'),
//    '#description' => t('Specify the weight of this product'),
//    '#default_value' => $product->weight,
//    '#maxlength' => 128,
//    '#required' => TRUE,
//    '#weight' => -10,
//  );
//
//  // We add the form's #submit array to this button along with the actual submit
//  // handler to preserve any submit handlers added by a form callback_wrapper.
//  $submit = array();
//
//  if (!empty($form['#submit'])) {
//    $submit += $form['#submit'];
//  }
//
//  $form['actions']['submit'] = array(
//    '#type' => 'submit',
//    '#value' => t('Save product'),
//    '#submit' => $submit + array('commerce_product_product_form_submit'),
//  );
//
//  // We append the validate handler to #validate in case a form callback_wrapper
//  // is used to add validate handlers earlier.
//  $form['#validate'][] = 'commerce_product_product_form_validate';
//
//  return $form;
//}