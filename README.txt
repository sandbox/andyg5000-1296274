Commerce Package Module
=====================

This module provides package management for Drupal Commerce stores.

It currently does these things:

1. Provides a UI at admin/commerce/config/package that lets you determine which
   product types should have package management. If you turn on package management
   a package field is added to the product type.

To configure:
-------------

1. Install and enable the module.
2. Enable package management on the products types you want it on by visiting
   admin/commerce/config/package.

 